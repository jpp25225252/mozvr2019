var models = [
  "glTF format/bamboo.gltf",
  "glTF format/bridge_arch.gltf",
  "glTF format/bridge_straight.gltf",
  "glTF format/cactus_large.gltf",
  "glTF format/cactus_short.gltf",
  "glTF format/cactus_tall.gltf",
  "glTF format/campfire_large.gltf",
  "glTF format/campfire_small.gltf",
  "glTF format/campfireStones_blocks.gltf",
  "glTF format/campfireStones_rocks.gltf",
  "glTF format/canoe_paddle.gltf",
  "glTF format/canoe.gltf",
  "glTF format/cliffBrown_block.gltf",
  "glTF format/cliffBrown_blockHalf.gltf",
  "glTF format/cliffBrown_blockQuarter.gltf",
  "glTF format/cliffBrown_cornerInnerMid.gltf",
  "glTF format/cliffBrown_cornerInnerTop.gltf",
  "glTF format/cliffBrown_cornerMid.gltf",
  
  "glTF format/cliffBrown_cornerTop.gltf",
  
  "glTF format/cliffBrown_entrance.gltf",
  
  "glTF format/cliffBrown_half.gltf",
  
  "glTF format/cliffBrown_halfCorner.gltf",
  
  "glTF format/cliffBrown_halfCornerInner.gltf",
  
  "glTF format/cliffBrown_steps.gltf",
  
  "glTF format/cliffBrown_stepsCornerInner.gltf",
  
  "glTF format/cliffBrown_stepsEnd.gltf",
  
  "glTF format/cliffBrown_top.gltf",
  
  "glTF format/cliffBrown_waterfallMid.gltf",
  
  "glTF format/cliffBrown_waterfallTop.gltf",
  
  "glTF format/cliffBrown.gltf",
  
  "glTF format/cliffGrey_block.gltf",
  
  "glTF format/cliffGrey_blockHalf.gltf",
  
  "glTF format/cliffGrey_blockQuarter.gltf",
  
  "glTF format/cliffGrey_cornerInnerMid.gltf",
  
  "glTF format/cliffGrey_cornerInnerTop.gltf",
  
  "glTF format/cliffGrey_cornerMid.gltf",
  
  "glTF format/cliffGrey_cornerTop.gltf",
  
  "glTF format/cliffGrey_entrance.gltf",
  
  "glTF format/cliffGrey_half.gltf",
  
  "glTF format/cliffGrey_halfCorner.gltf",
  
  "glTF format/cliffGrey_halfCornerInner.gltf",
  
  "glTF format/cliffGrey_steps.gltf",
  
  "glTF format/cliffGrey_stepsCornerInner.gltf",
  
  "glTF format/cliffGrey_stepsEnd.gltf",
  
  "glTF format/cliffGrey_top.gltf",
  
  "glTF format/cliffGrey_waterfallMid.gltf",
  
  "glTF format/cliffGrey_waterfallTop.gltf",
  
  "glTF format/cliffGrey.gltf",
  
  "glTF format/fence_corner.gltf",
  
  "glTF format/fence_cornerRound.gltf",
  
  "glTF format/fence_cornerRoundStrong.gltf",
  
  "glTF format/fence_diagonal.gltf",
  
  "glTF format/fence_diagonalStrong.gltf",
  
  "glTF format/fence_double.gltf",
  
  "glTF format/fence_gate.gltf",
  
  "glTF format/fence_simple.gltf",
  
  "glTF format/fence_simpleHigh.gltf",
  
  "glTF format/fence_simpleLow.gltf",
  
  "glTF format/fence_strong.gltf",
  
  "glTF format/fence.gltf",
  
  "glTF format/flower_beige1.gltf",
  
  "glTF format/flower_beige2.gltf",
  
  "glTF format/flower_beige3.gltf",
  
  "glTF format/flower_blue1.gltf",
  
  "glTF format/flower_blue2.gltf",
  
  "glTF format/flower_blue3.gltf",
  
  "glTF format/flower_red1.gltf",
  
  "glTF format/flower_red2.gltf",
  
  "glTF format/flower_red3.gltf",
  
  "glTF format/grass_dense.gltf",
  
  "glTF format/grass.gltf",
  
  "glTF format/ground_dirt.gltf",
  
  "glTF format/ground_dirtRiver.gltf",
  
  "glTF format/ground_dirtRiverBanks.gltf",
  
  "glTF format/ground_dirtRiverCorner.gltf",
  
  "glTF format/ground_dirtRiverCornerBank.gltf",
  
  "glTF format/ground_dirtRiverCornerInner.gltf",
  
  "glTF format/ground_dirtRiverCrossing.gltf",
  
  "glTF format/ground_dirtRiverEnd.gltf",
  
  "glTF format/ground_dirtRiverEntrance.gltf",
  
  "glTF format/ground_dirtRiverRocks.gltf",
  
  "glTF format/ground_dirtRiverSide.gltf",
  
  "glTF format/ground_dirtRiverSideCorner.gltf",
  
  "glTF format/ground_dirtRiverT.gltf",
  
  "glTF format/ground_dirtRiverTile.gltf",
  
  "glTF format/ground_dirtRiverWater.gltf",
  
  "glTF format/hanging_moss.gltf",
  
  "glTF format/lily_large.gltf",
  
  "glTF format/lily_small.gltf",
  
  "glTF format/log_large.gltf",
  
  "glTF format/log_small.gltf",
  
  "glTF format/logs_stack.gltf",
  
  "glTF format/logs_stackLarge.gltf",
  
  "glTF format/mushroom_brown.gltf",
  
  "glTF format/mushroom_brownGroup.gltf",
  
  "glTF format/mushroom_brownTall.gltf",
  
  "glTF format/mushroom_red.gltf",
  
  "glTF format/mushroom_redGroup.gltf",
  
  "glTF format/mushroom_redTall.gltf",
  
  "glTF format/palm_large.gltf",
  
  "glTF format/palm_small.gltf",
  
  "glTF format/palmDetailed_large.gltf",
  
  "glTF format/palmDetailed_small.gltf",
  
  "glTF format/plant_bush.gltf",
  
  "glTF format/plant_bushDetailed.gltf",
  
  "glTF format/plant_bushLarge.gltf",
  
  "glTF format/plant_bushSmall.gltf",
  
  "glTF format/plant_flatLarge.gltf",
  
  "glTF format/plant_flatSmall.gltf",
  
  "glTF format/pumpkin.gltf",
  
  "glTF format/rock_large1.gltf",
  
  "glTF format/rock_large2.gltf",
  
  "glTF format/rock_large3.gltf",
  
  "glTF format/rock_large4.gltf",
  
  "glTF format/rock_large5.gltf",
  
  "glTF format/rock_large6.gltf",
  
  "glTF format/rock_small1.gltf",
  
  "glTF format/rock_small2.gltf",
  
  "glTF format/rock_small3.gltf",
  
  "glTF format/rock_small4.gltf",
  
  "glTF format/rock_small5.gltf",
  
  "glTF format/rock_small6.gltf",
  
  "glTF format/rock_small7.gltf",
  
  "glTF format/rock_small8.gltf",
  
  "glTF format/rock_small9.gltf",
  
  "glTF format/rock_smallFlat1.gltf",
  
  "glTF format/rock_smallFlat2.gltf",
  
  "glTF format/rock_smallFlat3.gltf",
  
  "glTF format/rock_smallTop1.gltf",
  
  "glTF format/rock_smallTop2.gltf",
  
  "glTF format/rock_tall1.gltf",
  
  "glTF format/rock_tall2.gltf",
  
  "glTF format/rock_tall3.gltf",
  
  "glTF format/rock_tall4.gltf",
  
  "glTF format/rock_tall5.gltf",
  
  "glTF format/rock_tall6.gltf",
  
  "glTF format/rock_tall7.gltf",
  
  "glTF format/rock_tall8.gltf",
  
  "glTF format/rock_tall9.gltf",
  
  "glTF format/rock_tall10.gltf",
  
  "glTF format/stone_block.gltf",
  
  "glTF format/stone_column.gltf",
  
  "glTF format/stone_large1.gltf",
  
  "glTF format/stone_large2.gltf",
  
  "glTF format/stone_large3.gltf",
  
  "glTF format/stone_large4.gltf",
  
  "glTF format/stone_large5.gltf",
  
  "glTF format/stone_large6.gltf",
  
  "glTF format/stone_obelisk.gltf",
  
  "glTF format/stone_small1.gltf",
  
  "glTF format/stone_small2.gltf",
  
  "glTF format/stone_small3.gltf",
  
  "glTF format/stone_small4.gltf",
  
  "glTF format/stone_small5.gltf",
  
  "glTF format/stone_small6.gltf",
  
  "glTF format/stone_small7.gltf",
  
  "glTF format/stone_small8.gltf",
  
  "glTF format/stone_small9.gltf",
  
  "glTF format/stone_smallFlat1.gltf",
  
  "glTF format/stone_smallFlat2.gltf",
  
  "glTF format/stone_smallFlat3.gltf",
  
  "glTF format/stone_smallTop1.gltf",
  
  "glTF format/stone_smallTop2.gltf",
  
  "glTF format/stone_statue.gltf",
  
  "glTF format/stone_tall1.gltf",
  
  "glTF format/stone_tall2.gltf",
  
  "glTF format/stone_tall3.gltf",
  
  "glTF format/stone_tall4.gltf",
  
  "glTF format/stone_tall5.gltf",
  
  "glTF format/stone_tall6.gltf",
  
  "glTF format/stone_tall7.gltf",
  
  "glTF format/stone_tall8.gltf",
  
  "glTF format/stone_tall9.gltf",
  
  "glTF format/stone_tall10.gltf",
  
  "glTF format/tent_detailedClosed.gltf",
  
  "glTF format/tent_detailedOpen.gltf",
  
  "glTF format/tent_smallClosed.gltf",
  
  "glTF format/tent_smallOpen.gltf",
  
  "glTF format/tree_blocks_dark.gltf",
  
  "glTF format/tree_blocks_fall.gltf",
  
  "glTF format/tree_blocks.gltf",
  
  "glTF format/tree_default_dark.gltf",
  
  "glTF format/tree_default_fall.gltf",
  
  "glTF format/tree_default.gltf",
  
  "glTF format/tree_detailed_dark.gltf",
  
  "glTF format/tree_detailed_fall.gltf",
  
  "glTF format/tree_detailed.gltf",
  
  "glTF format/tree_oak_dark.gltf",
  
  "glTF format/tree_oak_fall.gltf",
  
  "glTF format/tree_oak.gltf",
  
  "glTF format/tree_pine_short_detailed.gltf",
  
  "glTF format/tree_pine_short.gltf",
  
  "glTF format/tree_pine_shortSquare_detailed.gltf",
  
  "glTF format/tree_pine_shortSquare.gltf",
  
  "glTF format/tree_pine_tall_detailed.gltf",
  
  "glTF format/tree_pine_tall.gltf",
  
  "glTF format/tree_pine_tallSquare_detailed.gltf",
  
  "glTF format/tree_pine_tallSquare.gltf",
  
  "glTF format/tree_pineSmall_round1.gltf",
  
  "glTF format/tree_pineSmall_round2.gltf",
  
  "glTF format/tree_pineSmall_round3.gltf",
  
  "glTF format/tree_pineSmall_round4.gltf",
  
  "glTF format/tree_pineSmall_round5.gltf",
  
  "glTF format/tree_pineSmall_square1.gltf",
  
  "glTF format/tree_pineSmall_square2.gltf",
  
  "glTF format/tree_pineSmall_square3.gltf",
  
  "glTF format/tree_plateau_dark.gltf",
  
  "glTF format/tree_plateau_fall.gltf",
  
  "glTF format/tree_plateau.gltf",
  
  "glTF format/tree_short_dark.gltf",
  
  "glTF format/tree_short_fall.gltf",
  
  "glTF format/tree_short.gltf",
  
  "glTF format/tree_simple_dark.gltf",
  
  "glTF format/tree_simple_fall.gltf",
  
  "glTF format/tree_simple.gltf",
  
  "glTF format/tree_small_dark.gltf",
  
  "glTF format/tree_small_fall.gltf",
  
  "glTF format/tree_small.gltf",
  
  "glTF format/tree_tall_dark.gltf",
  
  "glTF format/tree_tall_fall.gltf",
  
  "glTF format/tree_tall.gltf",
  
  "glTF format/tree_thin_dark.gltf",
  
  "glTF format/tree_thin_fall.gltf",
  
  "glTF format/tree_thin.gltf",
  
  "glTF format/treeStump_deep_side.gltf",
  
  "glTF format/treeStump_deep.gltf",
  
  "glTF format/treeStump_old.gltf",
  
  "glTF format/treeStump_round_side.gltf",
  
  "glTF format/treeStump_round.gltf",
  
  "glTF format/treeStump_side.gltf",
  
  "glTF format/treeStump.gltf",
  
  "glTF format/wheat.gltf"
]

loadModels()
instantiateModels()

function loadModels(){
  var asetsManager = document.getElementsByTagName('a-assets')[0]
  models.forEach((el,index) => {
    if(index < 30){
      asetsManager.innerHTML += '<a-asset-item id="model'+index+'" src="'+el+'"></a-asset-item>'
    }
  })
}

function instantiateModels() {
  var scene = document.getElementsByTagName('a-scene')[0]
  var cols = 6
  var rows = models.length / cols  
  models.forEach((el,index) => {
    if(index < 0{
      scene.innerHTML += '<a-gltf-model src="#model'+index+'" gltf-model="#model'+index+'" position="'+(index%cols)+' '+(index/cols)+' 0"></a-gltf-model>'
    }
  })

}